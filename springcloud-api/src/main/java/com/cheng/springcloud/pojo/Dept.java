package com.cheng.springcloud.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @ClassName Dept
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/13 16:58
 */
@Data
@NoArgsConstructor//无参
@Accessors(chain = true) //链式写法,支持dept.set().set()
public class Dept implements Serializable {/*序列化*/
    private Long deptno;
    private String dname;
    //判断此数据是属于哪个数据库,微服务一个服务对应一个数据库
    private String db_source;

    public Dept(String dname) {
        this.dname = dname;
    }
}
