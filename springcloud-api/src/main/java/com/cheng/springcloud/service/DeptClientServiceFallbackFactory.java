package com.cheng.springcloud.service;

import com.cheng.springcloud.pojo.Dept;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**服务降级
 * @ClassName DeptClientServiceFallbackFactory
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/14 21:26
 */
@Component
public class DeptClientServiceFallbackFactory implements FallbackFactory {
    @Override
    public DeptClientService create(Throwable cause) {
        return new DeptClientService() {
            @Override
            public Dept queryById(Long id) {
                return new Dept().setDeptno(id).setDname("id=>" + id + "没有对应的数据,客户端提供了降级服务,这个服务以关闭").setDb_source("没有数据");
            }

            @Override
            public List<Dept> queryAll() {
                return null;
            }

            @Override
            public Boolean addDept(Dept dept) {
                return null;
            }
        };
    }
}
