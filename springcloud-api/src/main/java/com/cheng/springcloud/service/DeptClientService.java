package com.cheng.springcloud.service;

import com.cheng.springcloud.pojo.Dept;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**feign通信接口
 * @ClassName DeptClientService
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/14 16:58
 */
@FeignClient(value = "SPRINGCLOUD-PROVIDER-DEPT",fallbackFactory = DeptClientServiceFallbackFactory.class)//服务名
public interface DeptClientService {

    @GetMapping("/dept/get/{id}")
    Dept queryById(@PathVariable("id") Long id);

    @GetMapping("/dept/queryAll")
    List<Dept> queryAll();

    @GetMapping("/dept/add")
    Boolean addDept(Dept dept);
}
