package com.cheng.springcloud.controller;

import com.cheng.springcloud.pojo.Dept;
import com.cheng.springcloud.service.DeptService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName DeptController
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/13 18:02
 */
@RestController
public class DeptController {
    @Autowired
    private DeptService deptService;

    @HystrixCommand(fallbackMethod = "HystrixGet")//指定该服务熔断后的解决方案
    @GetMapping("/dept/get/{id}")
    public Dept get(@PathVariable("id") Long id) {
        Dept dept = deptService.queryById(id);
        if (dept == null) {
            throw new RuntimeException(id + "->找不到数据-----");
        }
        return dept;
    }

    //解决方案
    public Dept HystrixGet(@PathVariable("id") Long id) {
        return new Dept().setDeptno(id).setDname(id + "->找不到数据-----null").setDb_source("没有数据库");
    }
}
