package com.cheng.springcloud;

import com.cheng.myrule.ChengRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @ClassName DeptController_80
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/13 19:03
 */
@SpringBootApplication
@EnableEurekaClient
@RibbonClient(value = "SPRINGCLOUD-PROVIDER-DEPT",configuration = ChengRule.class)//自定义负载均衡算法
public class DeptController_80 {
    public static void main(String[] args) {
        SpringApplication.run(DeptController_80.class, args);
    }
}
