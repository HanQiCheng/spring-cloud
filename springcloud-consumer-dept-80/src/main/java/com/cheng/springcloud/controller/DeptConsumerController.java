package com.cheng.springcloud.controller;

import com.cheng.springcloud.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import java.util.List;

/**
 * @ClassName DeptConsumerController
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/13 18:35
 */
@RestController
public class DeptConsumerController {

    @Autowired
    private RestTemplate restTemplate;//提供多种便捷访问远程http服务的方法，简单的Restful服务模板
    //private static final String PEST_URL_PREFIX = "http://localhost:8001";单机
    private static final String PEST_URL_PREFIX = "http://SPRINGCLOUD-PROVIDER-DEPT";//集群

    @RequestMapping("/controller/dept/get/{id}")
    public Dept get(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PEST_URL_PREFIX+"/dept/get/" + id, Dept.class);
    }

    @RequestMapping("/controller/dept/add")
    public Boolean addDept(Dept dept) {
        return restTemplate.postForObject(PEST_URL_PREFIX+"/dept/add", dept,Boolean.class);
    }

    @RequestMapping("/controller/dept/queryAll")
    public List queryAll() {
        return restTemplate.getForObject(PEST_URL_PREFIX+"/dept/queryAll", List.class);
    }
}
