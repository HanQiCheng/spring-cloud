package com.cheng.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**自定义负载均衡算法
 * @ClassName ChengRule
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/14 15:42
 */
@Configuration
public class ChengRule {
//负载均衡算法修改为随机模式
//    @Bean
//    public IRule myRule() {
//        return new RandomRule();
//    }

    /**
     * 官方算法:
     * RandomRule:随机
     * RoundRobinRule:轮循
     * AvailabilityFilteringRule :先过滤掉跳闸,访问故障的服务,对剩下的进行轮循
     * RetryRule:先按照轮询获取服务,获取失败则在指定时间内进行
     * @return
     */
    @Bean//自定义负载均衡
    public IRule CustomLoadBalancingRules() {
        return new CustomLoadBalancingRules();
    }
}
