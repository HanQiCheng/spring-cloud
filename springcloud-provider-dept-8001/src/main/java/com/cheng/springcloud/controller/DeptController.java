package com.cheng.springcloud.controller;

import com.cheng.springcloud.pojo.Dept;
import com.cheng.springcloud.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @ClassName DeptController
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/13 18:02
 */
@RestController
public class DeptController {
    @Autowired
    private DeptService deptService;

    //得到具体的微服务,获得一些配置信息
    @Autowired
    private DiscoveryClient client;


    @PostMapping("/dept/add")
    public boolean addDept(@RequestBody Dept dept) {
        return deptService.addDept(dept);
    }

    @GetMapping("/dept/get/{id}")
    public Dept get(@PathVariable Long id) {
        return deptService.queryById(id);
    }


    @GetMapping("/dept/queryAll")
    public List<Dept> queryAll() {
        return deptService.queryAll();
    }

    @RequestMapping("/dept/discovery")
    public Object Discovery() {
        //获取微服务清单
        List<String> list = client.getServices();
        System.out.println(list);
        //得到一个具体的微服务,通过具体的微服务id-ApplicationName
        List<ServiceInstance> instances1 = client.getInstances("SPRINGCLOUD-PROVIDER-DEPT");
        for (ServiceInstance serviceInstance : instances1) {
            System.out.println(serviceInstance.getHost() + "\t" +
                    serviceInstance.getPort()+ "\t" +
                    serviceInstance.getUri()+ "\t" +
                    serviceInstance.getServiceId());
        }
        return this.client;
    }

}
