package com.cheng.springcloud.service;

import com.cheng.springcloud.pojo.Dept;

import java.util.List;

/**
 * @ClassName DeptService
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/13 17:57
 */
public interface DeptService {
    
    boolean addDept(Dept dept);

    Dept queryById(Long id);

    List<Dept> queryAll();
}
