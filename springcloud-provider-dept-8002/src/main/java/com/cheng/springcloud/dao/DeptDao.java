package com.cheng.springcloud.dao;

import com.cheng.springcloud.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @ClassName DeptDao
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/13 17:43
 */
@Mapper
@Repository
public interface DeptDao {
    /**
     * 添加部门
     *
     * @param dept 预添加数据对象
     * @return boolean 判断是否增加成功
     */
    boolean addDept(Dept dept);

    Dept queryById(Long id);

    List<Dept> queryAll();

}
