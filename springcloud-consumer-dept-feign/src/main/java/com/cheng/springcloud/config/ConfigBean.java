package com.cheng.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @ClassName ConfigBean
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/13 18:40
 */
@Configuration//相当于spring主配置文件
public class ConfigBean {
    /**
     *将RestTemplate放到spring容器,通信组件
     * @return RestTemplate对象
     */
    @Bean
    @LoadBalanced   //开启负载均衡,默认轮循算法
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
