package com.cheng.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @ClassName FeignDeptController_80
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/13 19:03
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.cheng.springcloud"})
public class FeignDeptController_80 {
    public static void main(String[] args) {
        SpringApplication.run(FeignDeptController_80.class, args);
    }
}
