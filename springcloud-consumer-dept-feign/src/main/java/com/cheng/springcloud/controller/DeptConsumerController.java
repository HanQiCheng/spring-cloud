package com.cheng.springcloud.controller;

import com.cheng.springcloud.pojo.Dept;
import com.cheng.springcloud.service.DeptClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**Feign方式
 * @ClassName DeptConsumerController
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/13 18:35
 */
@RestController
public class DeptConsumerController {

    @Autowired
    private DeptClientService service = null;

    @RequestMapping("/controller/dept/get/{id}")
    public Dept get(@PathVariable("id") Long id) {
        return this.service.queryById(id);
    }

    @RequestMapping("/controller/dept/add")
    public Boolean addDept(Dept dept) {
        return this.service.addDept(dept);
    }

    @RequestMapping("/controller/dept/queryAll")
    public List queryAll() {
        return this.service.queryAll();
    }
}
