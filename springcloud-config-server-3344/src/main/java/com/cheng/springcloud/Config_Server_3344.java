package com.cheng.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @ClassName Config_Server_3344
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/15 15:59
 */
@EnableConfigServer   //开启配置中心
@SpringBootApplication
public class Config_Server_3344 {
    public static void main(String[] args) {
        SpringApplication.run(Config_Server_3344.class, args);
    }
}
