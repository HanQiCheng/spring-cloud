package com.cheng.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @ClassName ZuulApplication_9527
 * @Author: HQC_韩棋铖
 * @DateTime: 2021/4/15 14:17
 */
@SpringBootApplication
@EnableZuulProxy   //开启路由管理
public class ZuulApplication_9527 {
    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication_9527.class, args);
    }
}
